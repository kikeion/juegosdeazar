// version 1.0
funcion figura<-numcarta(num) // devuelve un n�mero
	Definir figura como texto;
	segun azar(13) + 2 Hacer
		2: figura<-"el dos";
		3: figura<-"el tres";
		4: figura<-"el cuatro";
		5: figura<-"el cinco";
		6: figura<-"el seis";
		7: figura<-"el siete";
		8: figura<-"el ocho";
		9: figura<-"el nueve";
		10: figura<-"el diez";
		11: figura<-"la Jota";
		12: figura<-"la Reina";
		13: figura<-"el Rey";
		14: figura<-"el As";
	FinSegun
FinFuncion

Funcion palo<-palocarta(num)
	Definir palo como texto;
	Segun azar(4) + 1 Hacer
		1: palo<-"de Corazones";
		2: palo<-"de Picas";
		3: palo<-"de Tr�boles";
		4: palo<-"de Rombos";
	FinSegun	
FinFuncion

Funcion carta<-apostado() //funci�n que recibe el dinero disponible y devuelve el importe apostado
	Definir apuestaInicial como entero;
	Repetir
		Escribir "Por favor, introduce 100 euros para jugar";
		Leer apuestaInicial;
	Hasta Que apuestaInicial = 100
	Escribir "Tienes ", apuestaInicial, " euros.";
FinFuncion
	
Funcion finalizar<-salir()
	Definir finalizar como logico;
	Definir r Como Caracter;
	finalizar <- Falso;
	Escribir "Desea finalizar? S/N";
	Leer r;
	Si r=="s" || r=="S" Entonces
		finalizar <- Verdadero;
	FinSi
FinFuncion


Algoritmo azar_cartas
	Definir ncarta1, ncarta2, pcarta1, pcarta2 como texto; 
	Definir apuestaInicial, apuesta, num como entero;
	Definir finalizar Como Logico;
	Definir jugar como texto;
	
	
	
	Escribir "BIENVENIDO AL JUEGO DE CARTAS.";
	Escribir "La carta m�s alta gana";
	Escribir "-----------$----------------";
	apuestaInicial <- apostado();
	apuestaInicial <- 100;
	num <- 0;
	
	Repetir
		Escribir "Sacamos la primera carta...";
		ncarta1 <- numcarta(num);
		pcarta1 <- palocarta(num);	
		Esperar 2 Segundos;
		Escribir "Has sacado ", ncarta1, " ", pcarta1;
		
		Escribir "Introduce tu apuesta: ";
		Leer apuesta;
		
		Escribir "Sacamos la segunda carta...";
		ncarta2 <- numcarta(num);
		pcarta2 <- palocarta(num);	
		Esperar 2 segundos;
		Escribir ncarta2, " ", pcarta2;
		
		Si (ncarta1) > (ncarta2) Entonces
			Escribir "Enhorabuena, has ganado";
			apuestaInicial <- apuestaInicial + apuesta;
		SiNo
			Escribir "Lo siento, has perdido";
			apuestaInicial <- apuestaInicial - apuesta;
		FinSi
		
		Escribir "Tienes ", apuestaInicial, " euros.";
		Si apuestaInicial <= 0 Entonces
			Escribir "Lo siento, no te queda m�s dinero para apostar";
		FinSi
		finalizar<-salir();
		Si finalizar = falso Entonces
			apuestaInicial <- apostado();
			apuestaInicial <- 100;
		FinSi
		
	Hasta Que finalizar = Verdadero O apuestaInicial <=0;
	
	Escribir "Hasta pronto!";
	
	
	
	
	
	
	
FinAlgoritmo
